package com.troii.bitbucketlink

import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.roots.ProjectFileIndex
import com.intellij.openapi.vcs.AbstractVcs
import com.intellij.openapi.vcs.actions.VcsContextFactory
import com.intellij.vcsUtil.VcsUtil

import java.awt.*
import java.awt.datatransfer.StringSelection

class BitbucketLinkAction extends DumbAwareAction {

	private static final Logger LOG = Logger.getInstance(BitbucketLinkAction.class);

	BitbucketLinkAction() {
		super("Bitbucket Link")
	}

	@Override
	void update(AnActionEvent anActionEvent) {
		super.update(anActionEvent)
		def project = anActionEvent.getData(PlatformDataKeys.PROJECT)
		def file = anActionEvent.getRequiredData(CommonDataKeys.VIRTUAL_FILE)
		def filePath = VcsContextFactory.SERVICE.instance.createFilePathOn(file)
		anActionEvent.presentation.enabled = project && filePath && VcsUtil.getVcsFor(project, filePath)
	}

	@Override
	void actionPerformed(AnActionEvent anActionEvent) {
		def file = anActionEvent.getRequiredData(CommonDataKeys.VIRTUAL_FILE)
		final Editor editor = anActionEvent.getRequiredData(CommonDataKeys.EDITOR)
		if (file && editor) {
			def project = editor.project
			if (project) {
				def module = ProjectFileIndex.SERVICE.getInstance(project).getModuleForFile(file)
				def filePath = VcsContextFactory.SERVICE.instance.createFilePathOn(file)
				def vcs = VcsUtil.getVcsFor(project, filePath)
				if (vcs) {
					def revisionNumber = vcs.vcsHistoryProvider.createSessionFor(filePath).revisionList.
							first().revisionNumber
					def baseUrl = new BitbucketRepo(module.moduleFile.parent.path).bitbucketBaseUrl
					def subpath = file.path - module.moduleFile.parent.path
					if (!baseUrl) {
						baseUrl = new BitbucketRepo(project.basePath).bitbucketBaseUrl
						subpath = file.path - project.baseDir.path
					}
					if (baseUrl) {
						def url = baseUrl + "src/" + revisionNumber + subpath + "#" + file.name + "-" + (editor.caretModel.logicalPosition.line + 1)
						CopyPasteManager.instance.contents = new StringSelection(url)
						openBrowser(url)
					}
				}
			}
		}
	}

	private void openBrowser(String url) {
		try {
			Desktop.getDesktop().browse(new URI(url))
		} catch (IOException e) {
			LOG.error("Unable to open browser for url: " + url, e)
		} catch (URISyntaxException e) {
			LOG.error("Unable to open browser for url: " + url, e)
		}
	}

}
