package com.troii.bitbucketlink;

import com.google.common.annotations.VisibleForTesting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BitbucketRepo {

	private static Pattern INI_CATEGORY = Pattern.compile("\\[\\s*(\\w+)[\\s'\"]+(\\w+)[\\s'\"]+\\]");
	private static Pattern URL_VALUE = Pattern.compile("\\s*url\\s*=\\s*([^\\s]*)\\.git");

	private final File gitConfigFile;

	BitbucketRepo(String repositoryRoot) {
		this(repositoryRoot, ".git/config");
	}

	@VisibleForTesting
	BitbucketRepo(String repositoryRoot, String gitconfig) {
		gitConfigFile = new File(repositoryRoot, gitconfig);
	}

	public String getBitbucketBaseUrl() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(gitConfigFile));
			String line;
			boolean inRemoteOriginSection = false;
			while ((line = reader.readLine()) != null) {
				if (line.matches("\\s*#")) continue;
				Matcher matcher = INI_CATEGORY.matcher(line);
				if (matcher.matches()) {
					inRemoteOriginSection = "remote".equals(matcher.group(1)) && "origin".equals(matcher.group(2));
					continue;
				}
				matcher = URL_VALUE.matcher(line);
				if (inRemoteOriginSection && matcher.matches()) {
					return "https://" + matcher.group(1).replaceAll("ssh://|git://|git@|https://", "").replaceAll(":", "/") + "/";
				}
			}
		} catch (IOException e) {
			// no config file found
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ignored) {
				}
			}
		}
		return null;
	}
}
