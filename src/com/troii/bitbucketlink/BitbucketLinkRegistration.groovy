package com.troii.bitbucketlink

import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.DefaultActionGroup
import com.intellij.openapi.actionSystem.IdeActions
import com.intellij.openapi.components.ApplicationComponent

class BitbucketLinkRegistration implements ApplicationComponent {

	@Override
	void initComponent() {
		def actionManager = ActionManager.instance
		def linkAction = new BitbucketLinkAction()
		actionManager.registerAction("BitbucketLinkAction", linkAction)
		def windowMenu = actionManager.getAction(IdeActions.GROUP_EDITOR_POPUP) as DefaultActionGroup
		windowMenu.addSeparator()
		windowMenu.add(linkAction)
	}

	@Override
	void disposeComponent() {

	}

	@Override
	String getComponentName() {
		return "BitbucketLink"
	}
}
